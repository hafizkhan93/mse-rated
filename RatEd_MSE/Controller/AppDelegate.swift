//
//  AppDelegate.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 23.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //HK: Tetsing Database
        FirebaseApp.configure()
        let colonsOnWrongSide: Int = 9
        print(colonsOnWrongSide) //just to satisfy lint
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}

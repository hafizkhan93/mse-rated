//
//  FavouriteUnisViewController.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 30.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class FavouriteUnisViewController: UITableViewController {
    let favouriteUniEntries = SampleData.generateUniTableEntries()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return favouriteUniEntries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let favouriteUniEntry = favouriteUniEntries[indexPath.row]
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: "favouriteUniCell", for: indexPath) as? UniEntryCell else {
            return tableView.dequeueReusableCell(withIdentifier: "favouriteUniCell", for: indexPath)
            /* todo: should return AlarmView instead!! Notify the user that there was a problem with the
             cell type
             https://stackoverflow.com/questions/36024543/is-force-cast-really-bad-and-should-always-avoid-it */
        }

        cell.favouriteUni = favouriteUniEntry
        // Configure the cell...
        return cell
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

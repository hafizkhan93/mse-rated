//
//  LoginController.swift
//  RatEd_MSE
//
//  Created by Juen Marcel on 25.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import Eureka
import Firebase
class LoginController: FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // swiftlint:disable empty_parentheses_with_trailing_closure
        form +++ Section("Account Data")
            <<< EmailRow() { row in
                row.title = "E-Mail"
                row.placeholder = "Enter E-Mail here"
                row.tag = "email"
            }
            <<< PasswordRow() {
                $0.title = "Password"
                $0.placeholder = "Enter Password here"
                $0.tag = "password"
            }
            <<< ButtonRow() { (row: ButtonRow) -> Void in
                row.title = "Login"
                }
                .onCellSelection { [weak self] (cell, row) in
                    cell.isHighlighted = true
                    row.isHighlighted = true
                    self?.login()
            }

    }
    
    @IBAction func login() {
        
        let email = getMailAddress()
        let password = getPassword()

        if (email.count > 0 && password.count > 0) {
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
                if let error = error, user == nil {
                    let alert = UIAlertController(title: "Sign In Failed",
                                                  message: error.localizedDescription,
                                                  preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    
                    self.present(alert, animated: true, completion: nil)
                } else {
                    //alert user if successful and pop view
                    let alert = UIAlertController(title: "Login successful", message: "You are logged in now!", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.navigationController?.popViewController(animated: true) //go back to previous ViewController (to My Profile Tab)
                    }))
                    self.present(alert, animated: true)
                }
            }
        } else {
            return
        }
        
    }
    
    //GETTER Methods
    
    func getMailAddress() -> String {
        //get Data from Form and check if nil
        if let mailRow = self.form.rowBy(tag: "email") as? EmailRow {
            if mailRow.value != nil {
                return mailRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    func getPassword() -> String {
        if let passwRow = self.form.rowBy(tag: "password") as? PasswordRow {
            if passwRow.value != nil {
                return passwRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
}

//
//  RegisterController.swift
//  RatEd_MSE
//
//  Created by Juen Marcel on 25.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//
// swiftlint: disable empty_parentheses_with_trailing_closure

import Eureka
import Firebase

struct RatedDatabase {
    
    static var ref = Database.database().reference(withPath: "user")
}
class RegisterController: FormViewController {
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        form +++ Section("Account Data")
            // swiftlint:disable empty_parentheses_with_trailing_closure
            <<< EmailRow() { row in
                row.title = "E-Mail"
                row.tag = "email"
                row.placeholder = "Enter E-Mail here"
            }
            <<< PasswordRow() {
                $0.title = "Password"
                $0.placeholder = "Enter Password here"
                $0.tag = "password"
            }
            +++ Section("Study Data")
            <<< PickerInputRow<String>("Uni") {
                $0.title = "University Name"
                $0.options = []
                for index in 1...10 {
                    $0.options.append("Uni \(index)")
                }
                $0.value = $0.options.first
                $0.tag = "university"
            }
            <<< PickerInputRow<String>("FieldOfStudy") {
                $0.title = "Field of Study"
                $0.options = []
                for index in 1...10 {
                    $0.options.append("Option \(index)")
                }
                $0.value = $0.options.first
                $0.tag = "fieldofstudy"
            }
            <<< DateRow() {
                $0.title = "Start Date"
                $0.value = Date(timeIntervalSinceReferenceDate: 0)
                $0.tag = "sdate"
            }
            // swiftlint:disable all
            <<< ButtonRow() { (row: ButtonRow) -> Void in
                row.title = "Register"
                }
                .onCellSelection { [weak self] (cell, row) in
                    self?.registerUser()
        }
    }
    
    
    
    @IBAction func registerUser() {
        //create User for Firebase Auth
        Auth.auth().createUser(withEmail: getMailAddress(), password: getPassword()) { user, error in
            if error != nil {
                let alert = UIAlertController(title: "Error", message: error.debugDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            } else {
                // create user entry in firebase database
                let newUser = UserReviewInfoSection(username: self.getMailAddress(), university: self.getUniversity(), startSemester: self.getStartDate(), fieldOfStudy: self.getFieldOfStudy())
                RatedDatabase.ref.childByAutoId().setValue(newUser.toAnyObject())
                
                //alert user if successful and pop view
                let alert = UIAlertController(title: "Registration successful", message: "You are registered now!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.navigationController?.popViewController(animated: true) //go back to previous ViewController (to My Profile Tab)
                }))
                
                //sign new user in
                Auth.auth().signIn(withEmail: self.getMailAddress(),
                                   password: self.getPassword())
                
                self.present(alert, animated: true)
            }
        }
    }
    
    //GETTER Methods
    
    func getMailAddress()  -> String {
        //get Data from Form and check if nil
        if let mailRow = self.form.rowBy(tag: "email") as? EmailRow {
            if mailRow.value != nil {
                return mailRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    func getPassword() -> String {
        if let passwRow = self.form.rowBy(tag: "password") as? PasswordRow {
            if passwRow.value != nil {
                return passwRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    func getFieldOfStudy() -> String {
        if let fosRow = self.form.rowBy(tag: "fieldofstudy") as? PickerInputRow<String> {
            if fosRow.value != nil {
                return fosRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    func getUniversity() -> String {
        if let uniRow = self.form.rowBy(tag: "university") as? PickerInputRow<String> {
            if uniRow.value != nil {
                return uniRow.value!
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    func getStartDate() -> String {
        if let sdateRow = self.form.rowBy(tag: "sdate") as? DateRow {
            if sdateRow.value != nil {
                //convert Date to String
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let myString = formatter.string(from: sdateRow.value!) // string purpose I add here
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd-MMM-yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                return myStringafd
            } else {
                let alert = UIAlertController(title: "Error", message: "Please fill in a correct start-date!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                return ""
            }
        } else {
            return ""
        }
    }
    
    // swiftlint:enable all
}

import UIKit

class ReviewTableViewController: UITableViewController, UITextViewDelegate {
    
    @IBOutlet var tbView: UITableView!
    @IBOutlet weak var textViewPro: UITextView!
    @IBOutlet weak var textViewContra: UITextView!
    @IBOutlet weak var textViewTips: UITextView!
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var tbViewCellPro: UITableViewCell!
    override func viewDidLoad() {
        super.viewDidLoad()
        tbView.keyboardDismissMode = .onDrag
        initToolbar()
        setPlaceholders()

    }
    
    func initToolbar() {
        //init toolbar
        let toolbar: UIToolbar =
            UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 30))
        
        //create left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let doneBtn: UIBarButtonItem =
            UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        
        //setting toolbar as inputAccessoryView
        textViewPro.inputAccessoryView = toolbar
        textViewContra.inputAccessoryView = toolbar
        textViewTips.inputAccessoryView = toolbar
    }
    
    func setPlaceholders() {
        textViewPro.text = "Placeholder for UITextView"
        textViewPro.textColor = UIColor.lightGray
        textViewPro.font = UIFont(name: "verdana", size: 13.0)
        textViewPro.delegate = self
        textViewContra.text = "Placeholder for UITextView"
        textViewContra.textColor = UIColor.lightGray
        textViewContra.font = UIFont(name: "verdana", size: 13.0)
        textViewContra.delegate = self
        textViewTips.text = "Placeholder for UITextView"
        textViewTips.textColor = UIColor.lightGray
        textViewTips.font = UIFont(name: "verdana", size: 13.0)
        textViewTips.delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Placeholder for UITextView" {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = UIFont(name: "verdana", size: 18.0)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Placeholder for UITextView"
            textView.textColor = UIColor.lightGray
            textView.font = UIFont(name: "verdana", size: 13.0)
        }
    }
    
    @objc func doneButtonAction() {
        textViewPro.endEditing(true)
    }
    
    func defineMessage(tag: Int) -> String {
        var msg = ""
        
        switch tag {
        case 1:
            msg = "do something when first button1 is tapped"
        case 2:
            msg = "do something when first button2 is tapped"
        case 3:
            msg = "do something when first button3 is tapped"
        case 4:
            msg = "do something when first button4 is tapped"
        default:
            msg = "Unknown Button Tag"
        }

        return msg
    }
    
    @IBAction func showInfo(_ sender: UIButton) {
        let alert = UIAlertController(title: "Info",
                                      message: defineMessage(tag: sender.tag),
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }

    @IBAction func cancelButtonTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

import UIKit
import Alamofire
import SwiftyJSON
import Firebase
class TopUnisViewController: UITableViewController {
    var uniTableEntries = [[String?]]()
    let UNIURL =
    "https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:UNIVERSITAETOGD&srsName=EPSG:4326&outputFormat=json"
    var index = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = false
        self.getJSONFile()
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                print(user?.email)
            }
            
        }
        
    }
    func getJSONFile() {
        let url = NSURL(string: UNIURL)
        //fetching the data from the url
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: {(data, _, _) -> Void in
            guard let data = data else {return}
            let json = SwiftyJSON.JSON(data)
            self.extractJSONInfos(json: json)
        }).resume()
    }
    func extractJSONInfos(json: JSON) {
        //helper Array to discard duplicates
        var uniqueArray = [String?]()
        let listOfUnis = json["features"]
        for (_, subJson): (String, JSON) in listOfUnis {
            let uniTitle = subJson["properties"]["NAME"].string
            
            if !uniqueArray.contains(uniTitle) {
                
                uniqueArray.append(uniTitle)
                let coordinates = subJson["geometry"]["coordinates"]
                let longitude =  String(coordinates[0].doubleValue)
                let latitude =  String(coordinates[1].doubleValue)
                let JSONuniData = [uniTitle, longitude, latitude]
                let uniDatabaseEntry = ["uniName": uniTitle,
                                        "long":longitude,
                                        "lat": latitude
                ]
                let reviewSample = ["byuser" : "",
                                    "date" : "",
                                    "lecturesquality":"",
                                    "facilitiesquality":"",
                                    "locationquality" :"",
                                    "socialquality"  :"",
                                    "pro" :"",
                                    "con" :"",
                                    "tips":""
                ]
                
                RatedDatabase.ref.database.reference(withPath:
                    "Data").child(String(index)).setValue(uniDatabaseEntry)
                RatedDatabase.ref.database.reference().child("Data/\(index)").child("Reviews").child("review").setValue(reviewSample)
                //provisory entry in Reviews
                
                index += 1
                
                self.uniTableEntries.append(JSONuniData)
            }
            
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return uniTableEntries.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print(uniTableEntries)
        let uniTableEntry = self.uniTableEntries[indexPath.row]
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: "TopUniCell", for: indexPath) as? UniEntryCell else {
                return tableView.dequeueReusableCell(withIdentifier: "TopUniCell", for: indexPath)
                /* todo: should return AlarmView instead!! Notify the user that there was a problem with the
                 cell type
                 https://stackoverflow.com/questions/36024543/is-force-cast-really-bad-and-should-always-avoid-it */
        }
        // Configure the cell..
        if let newUniEntry = uniTableEntry[0] {
            let newEntry =
                UniData(uniTitle: newUniEntry, numberOfReviews: 3, overallScore: 3, savingFrom: .favourites("iwas"))
            cell.topUni = newEntry
            return cell
        }
        return UITableViewCell()
    }
}

//
//  UniDetailViewController.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 09.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit
import Charts
import Firebase

class UniDetailViewController: UITableViewController {
    
    var item = [UniViewSection]()
    var cell: UITableViewCell = UITableViewCell()
    var viewModel: UniViewModel = UniViewModel()
    var index = 1
    var rowsForSectionOne = 3
    var rowsForSectionTwo = 4
    var userLoggedIn = false
    var userLoggedInMessage = ""
    
    override func viewDidLoad() {
        
        tableView.register(UserReviewInfoTableViewCell.nib, forCellReuseIdentifier: "UserInfoCell")
        
        tableView.register(UserReviewEntryTableViewCell.nib, forCellReuseIdentifier: "UserReviewEntryCell")
        
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.userLoggedIn = true
            } else {
                self.userLoggedIn = false
            }
        }
    }
    
    @IBAction func writeReviewButtonClicked(_ sender: Any) {

        if (userLoggedIn == true){
            self.performSegue(withIdentifier: "reviewSegue", sender: nil)
        } else {
            let alert = UIAlertController(title: "Please Register/Login", message: "Oops! Sorry, but you need an account to rate this university :)", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Register", style: .default, handler: { action in
                self.performSegue(withIdentifier: "registerSegue", sender: nil)
            }))
            alert.addAction(UIAlertAction(title: "Login", style: .default, handler: { action in
                self.performSegue(withIdentifier: "loginSegue", sender: nil)
            }))
            
            self.present(alert, animated: true)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 { return rowsForSectionOne } else { return rowsForSectionTwo }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        index = indexPath.row
        if indexPath.section == 1 {
            index = indexPath.row + 3
        }
        let item = viewModel.items[index % (rowsForSectionTwo + rowsForSectionOne)]
        switch item.type {
        case .uniGeneralInfoSection:
            if indexPath.section == 0 {
                if let cell =
                    tableView.dequeueReusableCell(
                        withIdentifier: "GeneralInfo", for: indexPath) as? UniGeneralInfoTableViewCell {
                    
                    cell.item = item
                    self.navigationItem.title = cell.uniTitleForNavigationBar
                    return cell
                }
            }
        case .uniImprintAndLocationInoSection:
            if indexPath.section == 0 {
                if let cell =
                    tableView.dequeueReusableCell(
                        withIdentifier: "LocationCell", for: indexPath) as? LocationTableViewCell {
                    cell.item = item
                    return cell
                }
            }
        case .uniRatingsSection:
            if indexPath.section == 0 {
                if let cell =
                    tableView.dequeueReusableCell(
                        withIdentifier: "PieChartCell", for: indexPath) as? PieChartsTableViewCell {
                    cell.item = item
                    return cell
                }
            }
        case .userReviewInfoSection:
            if indexPath.section == 1 {
                if let cell =
                    tableView.dequeueReusableCell(
                        withIdentifier: "UserInfoCell", for: indexPath) as? UserReviewInfoTableViewCell {
                    cell.item = item
                    return cell
                }
            }
        case .userReviewEntrySection:
            if indexPath.section == 1 && indexPath.row > 0 {
                guard let item = item as? UserReviewEntrySection else { return UITableViewCell() }
                if let cell =
                    tableView.dequeueReusableCell(
                        withIdentifier: "UserReviewEntryCell", for: indexPath) as?UserReviewEntryTableViewCell {
                    _=cell.correctIndexForReviewEntries(rightIndex: indexPath.row)
                    cell.item = item
                    return cell
                }
            }
        }
        return cell
        
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Most Recent Reviews"
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 && indexPath.row > 0 {
            return CGFloat(180.0)
        }
        return UITableViewAutomaticDimension
    }
}

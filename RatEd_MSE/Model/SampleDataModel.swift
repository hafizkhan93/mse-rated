//
//  SampleDataModel.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import Foundation

class SampleData {
    
    static func generateUniTableEntries() -> [UniData] {
        return [UniData(uniTitle: "TU Wien", numberOfReviews: 4, overallScore: 4, savingFrom: .topUni("top")),
                UniData(uniTitle: "Uni Wien", numberOfReviews: 3, overallScore: 3, savingFrom: .topUni("top")),
                UniData(uniTitle: "WU Wien", numberOfReviews: 2, overallScore: 4, savingFrom: .topUni("top"))
            
        ]
    }
    
}

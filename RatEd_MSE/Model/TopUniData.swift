//
//  UniTableEntry.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import Foundation

enum SavingFrom {
    case topUni(String)
    case favourites(String)
    func get() -> String {
        switch self {
        case .topUni(let topUni):
            return topUni
        case .favourites(let favouriteUni):
            return favouriteUni
        }
    }
}

struct UniData {
    //var uniImage: U
    var uniTitle: String
    var numberOfReviews: Int
    var overallScore: Int
    var savingFrom: SavingFrom
}

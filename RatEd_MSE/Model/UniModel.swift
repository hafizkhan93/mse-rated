//
//  UniModel.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import Foundation

class Uni {
    
    var uniTitle: String?
    var numberOfReviews: String?
    var uniLogoName: String?
    var numberOfStars: Int?
    var uniLocationStreet: String?
    var uniLocationCity: String?
    var longitude: Double?
    var latitude: Double?
    var lectureQuality: Double?
    var socialQuality: Double?
    var locationQuality: Double?
    var facilityQuality: Double?
    var overallScore: Double?
    
    //Initialiser Data from Alamofire and JSON Mapping
    // Currently it' false
    init?(uniTitle: String,
          numberOfReviews: String, uniLogoName: String,
          numberOfStars: Int,
          uniLocationStreet: String,
          uniLocationCity: String,
          lectureQuality: Double, socialQuality: Double,
          locationQuality: Double, facilityQuality: Double,
          overallScore: Double) {
        
           self.uniTitle = uniTitle
           self.numberOfReviews = "\(numberOfReviews) Reviews"
           self.uniLogoName = "TU-Signet"
           self.numberOfStars = numberOfStars
           self.uniLocationStreet = uniLocationStreet
           self.uniLocationCity = uniLocationCity
           self.lectureQuality = lectureQuality
           self.socialQuality = socialQuality
           self.locationQuality = locationQuality
           self.facilityQuality = facilityQuality
           self.overallScore = overallScore
    }
}

class UserEntity {
    
    var username: String?
    var startSemester: String?
    
}

class Review {
    
    var username: String?
    var fieldOfStudy: String?
    var startSemester: String?
    var pro: String?
    var con: String?
    var tips: String?
    var review = [String: String]()
   
    init?(username: String, startSemester: String, fieldOfStudy: String,
          pro: String, con: String, tips: String,
          review: [String: String]) {
        self.username = username
        self.startSemester = startSemester
        self.fieldOfStudy = fieldOfStudy
        self.pro = pro
        self.con = con
        self.tips = tips
        self.review = review
    }
    
}

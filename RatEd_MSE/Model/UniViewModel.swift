//
//  UniViewModel.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import Foundation
import UIKit
import Firebase

enum UniViewSectionType {
    
    case uniGeneralInfoSection
    case uniImprintAndLocationInoSection
    case uniRatingsSection
    case userReviewInfoSection
    case userReviewEntrySection
    
}

protocol UniViewSection {
    var type: UniViewSectionType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
    
}

extension UniViewSection {
    var rowCount: Int {
        return 1
    }
}

class UniGeneralInfoSection: UniViewSection {
    var type: UniViewSectionType {
        return .uniGeneralInfoSection
    }
    var sectionTitle: String {
        return "UniGeneralinfosection"
    }
    var rowCount: Int {
        return 3
    }
    
    var uniTitle: String
    var uniLogoName: String
    var numberOfStars: Int
    var numberOfReviews: String
    
    init(
        uniTitle: String,
        uniLogoName: String,
        numberOfStars: Int,
        numberOfReviews: String
        ) {
        self.uniTitle = "TU Wien"
        self.uniLogoName = uniLogoName
        self.numberOfStars = numberOfStars
        self.numberOfReviews = numberOfReviews
        
    }
    
}

class UniImprintAndLocationInfoSection: UniViewSection {
    var type: UniViewSectionType {
        return .uniImprintAndLocationInoSection
    }
    var sectionTitle: String {
        return "ImprintAndLocationInfo"
    }
    
    var uniTitle: String
    var uniStreetAdress: String
    var uniCityAdress: String
    
    init(uniTitle: String,
         uniStreetAdress: String, uniCityAdress: String) {
        self.uniTitle = uniTitle
        self.uniStreetAdress = uniStreetAdress
        self.uniCityAdress = uniCityAdress
    }
    
}

class UniRatingsSection: UniViewSection {
    
    var type: UniViewSectionType {
        return .uniRatingsSection
    }
    var sectionTitle: String {
        return "RatingsSection"
    }
    
    var lectureQuality: Double
    var socialQuality: Double
    var locationQuality: Double
    var facilityQuality: Double
    var overallScore: Double
    
    init(lectureQuality: Double,
         socialQuality: Double,
         locationQuality: Double,
         facilityQuality: Double,
         overallScore: Double
        ) {
        self.lectureQuality = lectureQuality
        self.socialQuality = socialQuality
        self.locationQuality = locationQuality
        self.facilityQuality = facilityQuality
        self.overallScore = overallScore
    }
}

class UserReviewInfoSection: UniViewSection {
    var type: UniViewSectionType {
        return .userReviewInfoSection
    }
    var sectionTitle: String {
        return "UserReviewInfosection"
    }
    var rowCount: Int {
        return 1
    }
    
    var username: String
    var university: String
    var startSemester: String
    var fieldOfStudy: String
    
    init(username: String, university: String, startSemester: String, fieldOfStudy: String) {
        self.username = username
        self.university = university
        self.startSemester = startSemester
        self.fieldOfStudy = fieldOfStudy
    }
    
    func toAnyObject() -> Any {
        let dictionary: NSDictionary = [
            "email": username,
            "university": university,
            "fieldofstudy": fieldOfStudy,
            "startdate": startSemester
        ]
        return dictionary
    }
}

class UserReviewEntrySection: UniViewSection {
    var type: UniViewSectionType {
        return .userReviewEntrySection
    }
    var sectionTitle: String {
        return "UserReviewEntrySection"
    }
    var rowCount: Int {
        return 1
    }
    
    var pro: String
    var con: String
    var tips: String
    var review = [String: String]()
    init(pro: String, con: String, tips: String, review: [String: String]) {
        self.pro = pro
        self.con = con
        self.tips = tips
        self.review = review
    }
    
}

class UniViewModel: NSObject {
    
    public var items = [UniViewSection]()
    
    override init() {
        super.init()
        //get the JSON Data ready for the View
guard let uni = Uni(uniTitle: "TU Wien",
                numberOfReviews: "3", uniLogoName: "TU-Signet",
                numberOfStars: 3, uniLocationStreet: "Karlsplatz 14",
                uniLocationCity: "1040 Wien",
                lectureQuality: 5.0, socialQuality: 5.0,
                locationQuality: 5.0, facilityQuality: 5.0,
                overallScore: 5.0)
            else { return }
        guard let review =
            Review(username: "hafizkhan", startSemester: "2017",
                   fieldOfStudy: "Informatik",
                   pro: "Mir hat fast alles gefallen",
                   con: "Schlechte Betreuung", tips: "Swift lernen",
                   review: ["Pro": "Mir hat fast alles gefallen",
        "Tips": "Swift lernen", "Con": "Die Tests waren zu schwer"])
            else { return }
        if let title = uni.uniTitle,
            let reviews = uni.numberOfReviews, let logo = uni.uniLogoName,
            let stars = uni.numberOfStars {
            let uniGeneralInfoSection = UniGeneralInfoSection(uniTitle: title,
uniLogoName: logo, numberOfStars: stars, numberOfReviews: reviews)
            items.append(uniGeneralInfoSection)
        }
        if let locationStreet = uni.uniLocationStreet,
            let locationCity = uni.uniLocationCity {
            let uniImprintAndLocationSection = UniImprintAndLocationInfoSection(uniTitle:
                uni.uniTitle!, uniStreetAdress:
                locationStreet, uniCityAdress:
                locationCity)
            
            items.append(uniImprintAndLocationSection)
            }
        if let cat1 = uni.lectureQuality, let cat2 = uni.socialQuality,
            let cat3 = uni.locationQuality, let cat4 = uni.facilityQuality,
            let cat5 = uni.overallScore {
            let uniRatingsSection =
                UniRatingsSection(lectureQuality: cat1,
                                  socialQuality: cat2, locationQuality: cat3,
                                  facilityQuality: cat4, overallScore: cat5)
            items.append(uniRatingsSection)
        }
        if let user = review.username, let userStudy = review.fieldOfStudy,
            let userStart = review.startSemester {
            let reviewInfoSection =
                UserReviewInfoSection(username: user, university: uni.uniTitle!, startSemester: userStart, fieldOfStudy: userStudy)
            items.append(reviewInfoSection)
        }
        if let reviewPro = review.pro, let reviewCon = review.con,
            let reviewTips = review.tips {
            let reviewEntries = review.review
            let reviewEntrySection =
UserReviewEntrySection(pro: reviewPro, con: reviewCon, tips: reviewTips, review: reviewEntries)
            let reviewArray = Array(reviewEntries.values).filter({$0 != ""})
            for _ in 0..<reviewArray.count {
                items.append(reviewEntrySection)
                }
            }
    }
}

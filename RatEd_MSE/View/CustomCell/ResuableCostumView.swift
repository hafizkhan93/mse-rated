//
//  ResuableCostumView.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 30.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class ResuableCostumView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
        
        let nibName = "UniEntryTableViewCell"
        var contentView: UIView?
    
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            guard let view = loadViewFromNib() else { return }
            view.frame = self.bounds
            self.addSubview(view)
            contentView = view
        }
        
        func loadViewFromNib() -> UIView? {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: nibName, bundle: bundle)
            return nib.instantiate(withOwner: self, options: nil).first as? UIView
        }
    

}

//
//  UniEntryTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 30.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UniEntryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var uniLogoImage: UIImageView!
    @IBOutlet weak var numberOfReviews: UILabel!
    @IBOutlet weak var numberOfStars: UIImageView!
    
    @IBOutlet weak var uniTitleLabel: UILabel!
    
    
    
    
    var uniItem: UniData? {
        didSet {
            
            guard let uniItem = uniItem  else { return }
            
            uniTitleLabel.text = uniItem.uniTitle
            numberOfReviews.text = String(uniItem.numberOfReviews)+" Reviews"
            numberOfStars.image = image(forNumberOfRatedScore: uniItem.overallScore)
            uniLogoImage.image = UIImage(named: "icons8-graduation-cap-50")
        }
    }
    
    func image(forNumberOfRatedScore score: Int) -> UIImage? {
        let imagename = "\(score)Stars"
        return UIImage(named: imagename)
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    
    
}

//
//  LocationTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 28.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleForImprintLabel: UILabel!
    @IBOutlet weak var streetAdressForImprintLabel: UILabel!
    @IBOutlet weak var cityAdressForImprintLabel: UILabel!
    
    var item: UniViewSection? {
        
        didSet {
            
            guard let item = item as? UniImprintAndLocationInfoSection else { return }
            
            titleForImprintLabel.text = item.uniTitle
            streetAdressForImprintLabel.text = item.uniStreetAdress
            cityAdressForImprintLabel.text = item.uniCityAdress
            }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

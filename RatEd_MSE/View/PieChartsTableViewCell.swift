//
//  UniDetailTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 24.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit
import Charts

class PieChartsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lectureQuality: PieChartView!
    @IBOutlet weak var socialQuality: PieChartView!
    @IBOutlet weak var locationQuality: PieChartView!
    @IBOutlet weak var facilityQuality: PieChartView!
    @IBOutlet weak var overallScore: PieChartView!
    
    var pieCharts = [PieChartView]()
    
    var item: UniViewSection? {
        
        didSet {
            pieChartUpdate()
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pieCharts = [lectureQuality, socialQuality,
                     locationQuality, facilityQuality, overallScore]
        
        //pieChartUpdate()
        
    }
    func pieChartUpdate() {
        
        let category1 = PieChartDataEntry(value: 80.0)
        let sum1 = PieChartDataEntry(value: 20.0)
        let dataSet1 = PieChartDataSet(values: [category1, sum1], label: nil)
        let data1 = PieChartData(dataSet: dataSet1)
        let colors = [UIColor.blue, UIColor.clear]
        
        dataSet1.selectionShift = 0
        dataSet1.valueColors = [UIColor.clear]
        dataSet1.colors = colors
        
        for pieChart in pieCharts {
            
            pieChart.animate(xAxisDuration: 4, yAxisDuration: 4)
            pieChart.holeRadiusPercent = 0.9
            pieChart.data = data1
            pieChart.chartDescription?.enabled = false
            pieChart.legend.enabled = false
            pieChart.holeColor = UIColor.clear
            let reachedReviewStars = Int((category1.value)/(100) * 5)
            pieChart.centerText = "\(reachedReviewStars)"+"/"+"\(5)"
            pieChart.tintColor = UIColor.black
            pieChart.notifyDataSetChanged()
        }
    }

}

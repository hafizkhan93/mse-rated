//
//  UniTableEntryCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UniTableEntryCell: UITableViewCell {

    @IBOutlet weak var uniTitle: String

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

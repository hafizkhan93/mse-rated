//
//  TopUniEntryCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 26.04.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UniEntryCell: UITableViewCell {
    @IBOutlet weak var uniImage: UIImageView!
    @IBOutlet weak var uniTitle: UILabel!
    @IBOutlet weak var numberOfReviews: UILabel!
    @IBOutlet weak var overallScore: UIImageView!
    
    var topUni: UniData? {
        didSet {
            guard let topUni = topUni  else { return }
            
            uniTitle.text = topUni.uniTitle
            numberOfReviews.text = String(topUni.numberOfReviews)+" Reviews"
            overallScore.image = image(forNumberOfRatedScore: topUni.overallScore)
            uniImage.image = UIImage(named: "icons8-graduation-cap-50")
        }
    }
    var favouriteUni: UniData? {
        didSet {
            guard let favouriteUni = favouriteUni else { return }
            
            uniTitle.text = favouriteUni.uniTitle
            numberOfReviews.text = String(favouriteUni.numberOfReviews)+" Reviews"
            overallScore.image = image(forNumberOfRatedScore: favouriteUni.overallScore)
            uniImage.image = UIImage(named: "icons8-graduation-cap-50")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    func image(forNumberOfRatedScore score: Int) -> UIImage? {
        let imagename = "\(score)Stars"
        return UIImage(named: imagename)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

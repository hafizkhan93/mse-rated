//
//  UniGeneralInfoTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 25.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UniGeneralInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var uniTitleLabel: UILabel!
    @IBOutlet weak var uniRatings: UIImageView!
    @IBOutlet weak var uniLogoImage: UIImageView!
    @IBOutlet weak var uniNumberOfReviews: UILabel!
    var uniTitleForNavigationBar: String?
    
    var item: UniViewSection? {
        didSet {
            guard let item = item as? UniGeneralInfoSection else { return }
            uniTitleForNavigationBar = item.uniTitle
            uniTitleLabel?.text? = item.uniTitle
            uniRatings?.image? = UIImage(named: "5Stars")!
            //uniLogoImage?.image? = UIImage(named: "TU-Signet")!
            uniNumberOfReviews?.text? = String(item.numberOfReviews)
            }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

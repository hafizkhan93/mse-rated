//
//  UserReviewEntryTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 28.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UserReviewEntryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reviewIndexLabel: UILabel!
    @IBOutlet weak var reviewText: UILabel!
    var correctIndex = 0
    var item: UniViewSection? {
        
        didSet {
            guard let item = item as? UserReviewEntrySection else { return }
            switch correctIndex {
            case 1:
                let proText = item.review["Pro"]
                reviewIndexLabel.text = "Pro"
                reviewText.text = proText
                
            case 2:
                let conText = item.review["Con"]
                reviewIndexLabel.text = "Con"
                reviewText.text = conText
                
            case 3:
                let tipsText = item.review["Tips"]
                reviewIndexLabel.text = "Tips"
                reviewText.text = tipsText
            default:
                print("ERROR")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    static var nib: UINib {
        
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    func correctIndexForReviewEntries(rightIndex: Int) -> Int {
        correctIndex = rightIndex
        return correctIndex
    }
}

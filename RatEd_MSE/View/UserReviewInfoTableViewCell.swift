//
//  ReviewTableViewCell.swift
//  RatEd_MSE
//
//  Created by Hafiz Khan on 24.05.18.
//  Copyright © 2018 Hafiz Khan. All rights reserved.
//

import UIKit

class UserReviewInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fieldOfStudyLabel: UILabel!
    @IBOutlet weak var semesterStart: UILabel!
    
    var item: UniViewSection? {
        didSet {
            guard let item = item as? UserReviewInfoSection else { return }
            
            usernameLabel.text = item.username
            fieldOfStudyLabel.text = item.fieldOfStudy
            semesterStart.text = item.startSemester
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}
